<DOCTYPE! html>
<head>
<meta charset="UTF-8">
<title>New Post</title>
<style>
body {
    width: 800px;
    margin: 0 auto;
    padding: 0;
    font:12px/16px Verdana, sans-serif;
}
</style>
</head>
<body>

<?php
session_start();
	if(isset($_POST['link'])){
        $link = $_POST['link'];}
        $title = $_POST['title'];
        $commentary = $_POST['commentary'];
        $user = $_SESSION['user'];

if($_SESSION['token'] !== $_POST['token']){
   die("Request forgery detected");
}

require 'database_connect.php';
        $stmt = $mysqli->prepare("Insert into posts (title, commentary, src_link, user) values (?, ?, ?, ?)");

        if (!$stmt){
            printf("Something went wrong; check code: %s\n", $mysqli->error);
                        exit;
        }


        $stmt->bind_param('ssss', $title, $commentary, $link, $user);

        $stmt->execute();
        $stmt->close();

header("Location: main.php");
exit;

?>

</body>
</html>
