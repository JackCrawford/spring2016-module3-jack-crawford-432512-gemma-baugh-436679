<DOCTYPE! html>
<head>
<meta charset="UTF-8">
<title><?php ?></title>
<style>
body {
    width: 800px;
    margin: 0 auto;
    padding: 0;
    font:12px/16px Verdana, sans-serif;
}
</style>
</head>
<body>

<?php
require 'navbar.php';

/*
show profile picture/icon
profile_icon field added with ENUM('A', 'B', 'C', 'D, 'E') NOT NULL DEFAULT 'A'
associate each letter with a picture saved on the server side (not using BLOB because it is good for long binary strings rather than image files)
(also, add ability to change icon and email from personal account page)
*/

require 'database_connect.php';
$stmt = $mysqli->prepare("select profile_icon from users where user=?");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
}
$stmt->bind_param('s', $view_user);

$stmt->execute();

$stmt->bind_result($profile_icon); //I believe this should work

$stmt->fetch();

$stmt->close();

//will compare new $profile_icon variable in conditional statements, then will echo image by source
if($profile_icon == 'A'){
echo '<img src="A.jpg">'; //Ok, need to get this on server
}

elseif($profile_icon == 'B'){
echo '<img src="B.jpg">';
}

elseif($profile_icon == 'C'){
echo '<img src="C.jpg">';
}

elseif($profile_icon == 'D'){
echo '<img src="D.jpg">';
}

echo '<img src="E.jpg">'; //could also pass another session or post variable


//show username
echo $_GET['view_user'];

$view_user = $_GET['view_user'];

$stmt = $mysqli->prepare("select email from users where user=?");

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
}
$stmt->bind_param('s', $view_user);

$stmt->execute();

$stmt->bind_result($email_user);

$stmt->fetch();

$stmt->close();
?>

<br><form action="MAILTO:'.$email_user.'" method="post" enctype="text/plain">
Send Message to <?php echo $view_user;  ?>:<br>
<textarea name="message" cols="50" rows="10"></textarea><br>
<label for="your_email">Your email address:</label><input type="email" name="your_email" id="your_email">
<input type="submit" value="Send" name="Send">
</form>

<?php
require 'database_connect.php';

$user = $_GET['view_user'];

$stmt = $mysqli->prepare("select title, src_link, posted, post_id from posts where user=? order by posted");
if(!$stmt){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}

$stmt->bind_param('s', $user);

$stmt->execute();

$result = $stmt->get_result();

while($row = $result->fetch_assoc()){

/*The following are meant to:
-provide post title which links to view post page
-provide post source if post includes a link
-provide poster which links to poster's profile and time posted
  */

echo '<a href="view_post.php?id='.$row["post_id"].'">'.$row["title"].'</a>';
echo ' Posted on '.$row["posted"].'';
echo '<a href="'.$row["src_link"].'">(Source)</a><br>';
}

$stmt->close();

?>


</body>
</html>
