<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Logout</title>
</head>
<body>

<?php
session_start();
session_destroy();

header("Location: main.php");
exit;
?>

</body>
</html>
