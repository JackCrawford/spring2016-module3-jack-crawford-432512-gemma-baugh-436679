<DOCTYPE! html>
<head>
<meta charset="UTF-8">
<title>My Posts</title>
<style>
body {
    width: 800px;
    margin: 0 auto;
    padding: 0;
    font:12px/16px Verdana, sans-serif;
}
</style>
</head>
<body>
    
<?php
$page = "user_account";
require 'navbar.php';
        
require 'database_connect.php';

$stmt = $mysqli->prepare("select title, src_link, user, posted, post_id from posts where user=? order by posted");
if(!$stmt){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}

$stmt->bind_param('s', $_SESSION['user']);

$stmt->execute();

$result = $stmt->get_result();

while($row = $result->fetch_assoc()){

/*The following are meant to:
-provide post title which links to view post page
-provide post source if post includes a link
-provide poster which links to poster's profile and time posted
  */

echo '<a href="view_post.php?id='.$row["post_id"].'">'.$row["title"].'</a>';
echo ' Posted on '.$row["posted"].'<br>';
if(isset($row["src_link"])){
echo ' ';
echo '<a href="'.$row["src_link"].'">(Source)</a>';
}
echo ' ';
echo '<a href="edit_post.php?id='.$row["post_id"].'">Edit Post</a><br>';
echo '<br>';
}

$stmt->close();

?>

</body>
</html>
