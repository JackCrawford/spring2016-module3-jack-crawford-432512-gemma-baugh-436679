<DOCTYPE! html>
<head>
<meta charset="UTF-8">
<title>Edit Post</title>
<style>
body {
    width: 800px;
    margin: 0 auto;
    padding: 0;
    font:12px/16px Verdana, sans-serif;
}
</style>
</head>
<body>


<?php
session_start();
/*
php taking the form data and deleting it from the posts table
*/
$post_id = $_SESSION['post_id'];

require 'database_connect.php';

//the following deletes posts when delete button is pressed

$stmt = $mysqli->prepare("delete from posts where post_id=?");

        if (!$stmt){
            printf("Something went wrong; check code: %s\n", $mysqli->error);
                        exit;
        }


        $stmt->bind_param('i', $post_id);

                $stmt->execute();

                $stmt->close();

header("Location: user_posts.php");
exit;


?>

</body>
</html>
