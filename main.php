<DOCTYPE! html>
<head>
<meta charset="UTF-8">
<title>Derp: Main</title>
<style>
body {
    width: 800px;
    margin: 0 auto;
    padding: 0;
    font:12px/16px Verdana, sans-serif;
}
</style>
</head>
<body>

<?php
//designate current page for use in navbar.php
$page = "main";
require 'navbar.php';

//display listing of posts (title + src_link)

require 'database_connect.php';
 
$stmt = $mysqli->prepare("select title, src_link, user, posted, post_id from posts order by posted");
if(!$stmt){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}
 
$stmt->execute();
 
$result = $stmt->get_result();

while($row = $result->fetch_assoc()){
/*
The following are meant to:
-provide post title which links to full post
-provide post source if post includes a link
-provide poster which links to poster's profile and time posted
  */

echo '<a href="view_post.php?id='.$row["post_id"].'">'.$row["title"].'</a>';
echo ' ';
echo '<a href="'.$row["src_link"].'">(Source)</a>';
echo ' ';
echo 'Posted by <a href="view_user.php?view_user='.$row["user"].'">'.$row["user"].'</a> on '.$row["posted"].'<br>';

}

$stmt->close();

?>
</body>
</html>
