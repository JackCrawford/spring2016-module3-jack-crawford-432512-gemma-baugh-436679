<DOCTYPE! html>
<head>
<meta charset="UTF-8">
<title>My Account</title>
<style>
body {
    width: 800px;
    margin: 0 auto;
    padding: 0;
    font:12px/16px Verdana, sans-serif;
}
</style>
</head>
<body>
    
<?php
$page = "user_account";
require 'navbar.php'
?>

<?php
$user = $_SESSION['user'];
require 'database_connect.php';
$stmt = $mysqli->prepare("select profile_icon from users where user=?");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
}
$stmt->bind_param('s', $user);

$stmt->execute();

$stmt->bind_result($profile_icon); //Setting new variable profile_icon

$stmt->fetch();

$stmt->close();

if($profile_icon == 'A'){
echo '<img src="A.jpg">'; //Ok, need to get this on server
}

elseif($profile_icon == 'B'){
echo '<img src="B.jpg">';
}

elseif($profile_icon == 'C'){
echo '<img src="C.jpg">';
}

elseif($profile_icon == 'D'){
echo '<img src="D.jpg">';
}
else{
echo '<img src="E.jpg">';
}

echo $user;

?>

<p> Select a radio button to pick a profile icon!  </p>
<form name ="Select Avatar:" method ="post" action ="user_account.php">

    <Input type = 'Radio' Name ='avatar' value= 'A'>

    <Input type = 'Radio' Name ='avatar' value= 'B'>
    
    <Input type = 'Radio' Name ='avatar' value= 'C'>
    
    <Input type = 'Radio' Name ='avatar' value= 'D'>
    
    <Input type = 'Radio' Name ='avatar' value= 'E'> 

    <Input type = "Submit" Name = "avatar_submit" value = "Choose Avatar">

</form>

<?php

if (isset($_POST['avatar_submit'])) {

    $selected_avatar = $_POST['avatar']; //http://www.homeandlearn.co.uk/php/php4p10.html

    if ($selected_avatar == 'A') {
    
        $stmt = $mysqli->prepare("UPDATE users SET profile_icon = 'A' where user=?");
        
        $stmt->bind_param('s', $user);
        
        $stmt->execute();
 
        $stmt->close();
        
        echo "Avatar Updated";

    }
    
    elseif ($selected_avatar == 'B') {
    
        $stmt = $mysqli->prepare("UPDATE users SET profile_icon = 'B' where user=?");
        
        $stmt->bind_param('s', $user);
        
        $stmt->execute();
 
        $stmt->close();
        
        echo "Avatar Updated";

    }
    
    elseif ($selected_avatar == 'C') {
    
        $stmt = $mysqli->prepare("UPDATE users SET profile_icon = 'C' where user=?");
        
        $stmt->bind_param('s', $user);
        
        $stmt->execute();
 
        $stmt->close();
        
        echo "Avatar Updated";
    
    }
    
    elseif ($selected_avatar == 'D') {
    
        $stmt = $mysqli->prepare("UPDATE users SET profile_icon = 'D' where user=?");
        
        $stmt->bind_param('s', $user);
        
        $stmt->execute();
 
        $stmt->close();
        
        echo "Avatar Updated";
    
    }
    
    else {
    
        $stmt = $mysqli->prepare("UPDATE users SET profile_icon = 'E' where user=?");
        
        $stmt->bind_param('s', $user);
        
        $stmt->execute();
 
        $stmt->close();
        
        echo "Avatar Updated";
    
    }

}

?>

<form name ="Change email:" method ="post" action ="user_account.php">
    <label for="email">Change email:</label>
                <input type="text" name="email" id="email" required>
    <Input type = "Submit" Name = "email_submit" value = "Submit">
</form>

<?php

    if (isset($_POST['email_submit'])) {
    
        $email = $_POST['email'];
        
        $stmt = $mysqli->prepare("UPDATE users SET email = ? where user=?");
        
        $stmt->bind_param('ss', $email, $user);
        
        $stmt->execute();
 
        $stmt->close();
    }

?>

<a href="user_posts.php">My Posts</a>

<a href="user_comments.php">My Comments</a>

</body>
</html>
