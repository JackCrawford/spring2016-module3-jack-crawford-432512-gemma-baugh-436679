<DOCTYPE! html>
<head>
<meta charset="UTF-8">
<title>Edit Post</title>
<style>
body {
    width: 800px;
    margin: 0 auto;
    padding: 0;
    font:12px/16px Verdana, sans-serif;
}
</style>
</head>
<body>

<?php
session_start();

$post_id = $_SESSION['post_id'];
$title = $_POST['new_title'];
$commentary = $_POST['new_commentary'];
$user = $_SESSION['user'];
$link = " ";
if(isset($_POST['new_link'])){
if(filter_var($_POST['new_link'], FILTER_VALIDATE_URL)){
$link = $_POST['new_link'];
}
}

if($_SESSION['token'] !== $_POST['token']){
   die("Request forgery detected");
}

require 'database_connect.php';

/* the following updates the posts table, with a title, commentary, and link changes */

$stmt = $mysqli->prepare("update posts set title=?, commentary=?, src_link=? where post_id=?");
if(!$stmt){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}

$stmt->bind_param('sssi', $title, $commentary, $link, $post_id);

$stmt->execute();

$stmt->close();

header("Location: user_posts.php");
exit;

?>
</body>
</html>
