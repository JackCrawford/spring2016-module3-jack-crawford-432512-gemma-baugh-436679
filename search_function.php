<DOCTYPE! html>
<head>
<meta charset="UTF-8">
<title>Derp: Main</title>
<style>
body {
    width: 800px;
    margin: 0 auto;
    padding: 0;
    font:12px/16px Verdana, sans-serif;
}
</style>
</head>
<body>
    
    <form action="search_function.php"  method="POST">
        <p> <label for="search">Your search:</label>
            <input type="text" name="search" id="search" required>
            <input type="submit" value="Search!">
        </p>
    </form>
    
<?php
session_start();

require 'database_connect.php';

if (isset($_POST['submit'])){
    if (isset($_POST['search'])){
        $search = $_POST['search'];
        
        $stmt = $mysqli->prepare("select title, src_link, user, posted, post_id from posts where title like '%?%' order by posted"); //not sure if this wildcard syntax is correct
        if(!$stmt){ //need to make sure searching database correctly based on $search variable
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
        
        $stmt->bind_param('s', $search);
        
        $stmt->execute();
 
        $result = $stmt->get_result();
        
        while($row = $result->fetch_assoc()){
            echo '<a href="view_post.php?id='.$row["post_id"].'">'.$row["title"].'</a>';
            if(isset($row["src_link"])){
            echo ' ';
            echo '<a href="'.$row["src_link"].'">(Source)</a>';
            }
            echo ' ';
            echo 'Posted by <a href="view_user.php?view_user='.$row["user"].'">'.$row["user"].'</a> on '.$row["posted"].'<br>';

        }

    $stmt->close();
    }
}

?>
    
</body>
</html>