<DOCTYPE! html>
<head>
<meta charset="UTF-8">
<title>Edit Post</title>
<style>
body {
    width: 800px;
    margin: 0 auto;
    padding: 0;
    font:12px/16px Verdana, sans-serif;
}
</style>
</head>
<body>

<?php
require 'navbar.php';
require 'database_connect.php';

if(isset($_GET['id'])){
$_SESSION['comment_id'] = $_GET['id'];
}
$comment_id = $_SESSION['comment_id'];
$_SESSION['token'] = "sup";

$stmt = $mysqli->prepare("select comment from comments where comment_id=?");
if(!$stmt){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}

$stmt->bind_param('i', $comment_id);

$stmt->execute();
 
$stmt->bind_result($original_comment);

$stmt->fetch();
$stmt->close();

?>
<form action="edit_comment.php" method="POST">
    <input type="hidden" name="delete" value="delete"/>
    <input type="submit" value="Delete Comment" name="DeleteComment" id="DeleteComment"/>
</form>

<form action="edit_comment.php" method="POST">
<br><textarea name="comment_box"><?php echo $original_comment; ?></textarea>
<input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
    <input type="submit" value="Edit Comment" name="Post" id="Post"/>
</form>

<?php

require 'database_connect.php';
if(isset($_POST['delete'])){
$stmt = $mysqli->prepare("delete from comments where comment_id=?");

        if (!$stmt){
            printf("Something went wrong; check code: %s\n", $mysqli->error);
                        exit;
        }


        $stmt->bind_param('i', $comment_id);

                $stmt->execute();

                $stmt->close();

header("Location: user_comments.php");
exit;
    
}

else if(isset($_POST['comment_box'])){

if($_SESSION['token'] !== $_POST['token']){
   die("Request forgery detected");
}

        $comment = $_POST['comment_box'];
        $stmt = $mysqli->prepare("update comments set comment=? where comment_id=?");

        if (!$stmt){
            printf("Something went wrong; check code: %s\n", $mysqli->error);
                        exit;
        }


        $stmt->bind_param('si', $comment, $comment_id);

                $stmt->execute();

                $stmt->close();
header("Location: user_comments.php");
exit;

    }
?>

</body>
</html>
