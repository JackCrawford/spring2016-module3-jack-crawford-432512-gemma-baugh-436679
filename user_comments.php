<DOCTYPE! html>
<head>
<meta charset="UTF-8">
<title>My Account</title>
<style>
body {
    width: 800px;
    margin: 0 auto;
    padding: 0;
    font:12px/16px Verdana, sans-serif;
}
</style>
</head>
<body>
    
<?php
$page = "user_account";
require 'navbar.php';

$user = $_SESSION['user'];

    require 'database_connect.php';
    $stmt = $mysqli->prepare("select post_id, comment_id, commented, comment from comments where user=? order by commented");
    
    if(!$stmt){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
    
    $stmt->bind_param('s', $user);
    
    $stmt->execute();
 
    $result = $stmt->get_result();
    
while($row = $result->fetch_assoc()){
/*
The following are meant to:
-provide post title which links to full post
-provide post source if post includes a link
-provide poster which links to poster's profile and time posted
  */
echo '<br><a href="view_post.php?id='.$row["post_id"].'">Original Post</a><br>';
echo ' ';
echo $row["comment"];
echo '<br>Comment posted on '.$row["commented"].'<br>';
echo '<a href="edit_comment.php?id='.$row["comment_id"].'">Edit</a><br>';

}

$stmt->close();

?>


</body>
</html>
