<DOCTYPE! html>
<head>
<meta charset="UTF-8">
<title>Edit Post</title>
<style>
body {
    width: 800px;
    margin: 0 auto;
    padding: 0;
    font:12px/16px Verdana, sans-serif;
}
</style>
</head>
<body>

<?php

require 'navbar.php';

$_SESSION['token'] = "sup";


if(isset($_GET['id'])){
$_SESSION['post_id'] = $_GET['id'];
}
$post_id = $_SESSION['post_id'];
$user = $_SESSION['user'];

require 'database_connect.php';

$stmt = $mysqli->prepare("select title, src_link, commentary, posted from posts where post_id=?");
if(!$stmt){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}


$stmt->bind_param('i', $post_id);
 
$stmt->execute();
 
$stmt->bind_result($title, $link, $commentary, $posted);

$stmt->fetch();

$stmt->close();

/*
form below with delete option and form with input fields for title, optional link, and commentary
 */
?>

<form action="delete_edit_post.php" method="POST">
    <input type="submit" value="Delete Post" name="DeletePost" id="DeletePost"/>
</form>

<?php
echo ' Posted on '.$posted.'';
?>

<form action="update_edit_post.php" method="post">
<p>        <label for="title">Title:</label>
                <textarea name="new_title" cols="50" rows="10" required><?php echo $title; ?></textarea> </p>
<p>        <label for="link">Link:</label>
                <textarea name="new_link" cols="50" rows="10"><?php echo $link; ?></textarea> </p>
<p>        <label for="commentary">Commentary:</label>
                <textarea name="new_commentary" cols="50" rows="10" required><?php echo $commentary; ?></textarea> </p>
<input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
                <input type="submit" value="Update" name="UpdatePost" id="UpdatePost">
</form>


<!--
/*
php taking the form data and adding it to the posts table
*/
require 'database_connect.php';

//the following deletes posts when delete button is pressed

if(isset($_POST['delete'])){

$stmt = $mysqli->prepare("delete from posts where post_id=?");

        if (!$stmt){
            printf("Something went wrong; check code: %s\n", $mysqli->error);
                        exit;
        }


        $stmt->bind_param('i', $post_id);

                $stmt->execute();

                $stmt->close();

header("Location: user_posts.php");
exit;

}

if(isset($_POST['title'])){
if(isset($_POST['commentary'])){

$title = $_POST['title'];
$commentary = $_POST['commentary'];
$user = $_SESSION['user'];

if(isset($_POST['link'])){
$link = $_POST['link'];
}

if($_SESSION['token'] !== $_POST['token']){
   die("Request forgery detected");
}

if(filter_var($link, FILTER_VALIDATE_URL)){

/* the following updates the posts table, with a title, commentary, and link changes */

$stmt = $mysqli->prepare("update posts set title=?, commentary=?, src_link=? where post_id=?");
if(!$stmt){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}

$stmt->bind_param('sssi', $title, $commentary, $link, $post_id);

$stmt->execute();

$stmt->close();

header("Location: user_posts.php");
exit;
}
}
}
-->

</body>
</html>
