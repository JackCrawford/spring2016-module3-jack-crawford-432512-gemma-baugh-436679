<DOCTYPE! html>
<head>
<meta charset="UTF-8">
<title>New Post</title>
<style>
body {
    width: 800px;
    margin: 0 auto;
    padding: 0;
    font:12px/16px Verdana, sans-serif;
}
</style>
</head>
<body>

<?php
$page = "main";
require 'navbar.php';
$_SESSION['token'] = "sup";
/*
form below with input fields for title, optional link, and commentary
 */
?>
<form action="submit_new_post.php" method="post">
<p>        <label for="title">Title:</label>
                <input type="text" name="title" id="title" required> </p>
<?php echo '<input type="hidden" name="token" value="'.$_SESSION['token'].'" />'; ?>
<p>        <label for="link">Link:</label>
                <input type="text" name="link" id="link" required>
     <label for="link">Include full link with https:// </label>
</p>
<p>        <label for="commentary">Commentary:</label>
                <textarea name="commentary" id="commentary" cols="50" rows="10" required></textarea> </p>
                <input type="submit" value="Post" name="Post" id="Post">
</form>

<!--
<?php

require 'database_connect.php';

if(isset($_POST['title'])){
if(isset($_POST['commentary'])){
if(isset($_POST['link'])){
   $link = $_POST['link'];
}
        $title = $_POST['title'];
        $commentary = $_POST['commentary'];
        $user = $_SESSION['user'];

if($_SESSION['token'] !== $_POST['token']){
   die("Request forgery detected");
}

        $stmt = $mysqli->prepare("Insert into posts (title, commentary, link, user) values (?, ?, ?, ?)");

        if (!$stmt){
            printf("Something went wrong; check code: %s\n", $mysqli->error);
                        exit;
        }


        $stmt->bind_param('ssss', $title, $commentary, $link, $user);

        $stmt->execute();
        $stmt->close();

header("Location: main.php");
exit;   

}
}
?>
-->

<!--
/*
php taking the form data and adding it to the posts table
*/
/*
if(isset($_POST['title'])){
if(isset($_POST['commentary'])){
if(isset($_POST['link'])){

$link = $_POST['link'];
$title = $_POST['title'];
$commentary = $_POST['commentary'];
$user = $_SESSION['user'];

require 'database_connect';
/* the following adds a new post to the posts table, with a title, commentary, link, and the poster username */
$stmt = $mysqli->prepare("insert into posts (title, commentary, src_link, user) values (?, ?, ?, ?)");
if(!$stmt){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}
 
$stmt->bind_param('ssss', $title, $commentary, $link, $user);
 
$stmt->execute();

// Update session variable with post_id for view post
$_SESSION['post_id'] = $mysqli->insert_id;

$stmt->close();

header("Location: view_post.php");
exit;
}

else{
require 'database_connect';
// the following adds a new post to the posts table, with a title, commentary, and the poster username but NO link
$stmt = $mysqli->prepare("insert into posts (title, commentary, user) values (?, ?, ?)");
if(!$stmt){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}

$stmt->bind_param('sss', $title, $commentary, $user);

$stmt->execute();

// Update session variable with post_id for view post
$_SESSION['post_id'] = $mysqli->insert_id;

$stmt->close();

header("Location: view_post.php");
exit;
}
}
}

-->

</body>
</html>
