<DOCTYPE! html>
<head>
<meta charset="UTF-8">
<title>View Post</title>
<style>
body {
    width: 800px;
    margin: 0 auto;
    padding: 0;
    font:12px/16px Verdana, sans-serif;
}
</style>
</head>
<body>

<?php

require 'navbar.php';

if(isset($_GET['id'])){
     $_SESSION['post_id'] = $_GET['id'];
     }
$post_id = $_SESSION['post_id'];
$user = $_SESSION['user'];

require 'database_connect.php';

$stmt = $mysqli->prepare("select title, src_link, commentary, user, posted from posts where post_id=?");
if(!$stmt){
printf("Query Prep Failed: %s\n", $mysqli->error);
exit;
}


$stmt->bind_param('s', $post_id);
 
$stmt->execute();
 
$stmt->bind_result($title, $link, $commentary, $poster, $posted);
 
$stmt->fetch();

    echo '<a href="view_post.php?id='.$post_id.'">'.$title.'</a><br>';
if(isset($link)){
echo '<a href="'.$link.'">(Source)</a><br>';
}
    printf("%s", htmlentities($commentary));
    echo '<br>Posted by <a href="view_user.php?view_user='.$poster.'">'.$poster.'</a> on '.$posted.'';

$stmt->close();
?>

<form action="view_post.php" method="POST">

        <br><textarea name="comment_box"></textarea>
<input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
    <input type="submit" value="Post Comment" id="Post"/>
</form>

<p>
Comments:
</p>

<?php

require 'database_connect.php';

if(isset($_POST['comment_box'])){
        $comment = $_POST['comment_box'];

if($_SESSION['token'] !== $_POST['token']){
   die("Request forgery detected");
}

        $stmt = $mysqli->prepare("Insert into comments (post_id, comment, user) values (?, ?, ?)");

        if (!$stmt){
            printf("Something went wrong; check code: %s\n", $mysqli->error);
                        exit;
        }
        

        $stmt->bind_param('sss', $post_id, $comment, $user);
	 
		$stmt->execute();
	 
		$stmt->close();    }
?>

<?php
//displaying comments (user who commented, timestamp, comment)

require 'database_connect.php';

$stmt = $mysqli->prepare("select user, commented, comment from comments where post_id = ? order by current_timestamp");
if(!$stmt){
printf("Error querying and displaying comments: %s\n", $mysqli->error);
exit;
}

$stmt->bind_param('s', $post_id);

$stmt->execute();

$result = $stmt->get_result();

while($row = $result->fetch_assoc()){

    echo ' ';
    echo 'Posted by <a href="view_user.php?view_user='.$row["user"].'">'.$row["user"].'</a> on '.$row["commented"].'<br>';
    printf("%s", htmlentities($row["comment"]));
    echo '<br><br>';

}

$stmt->close();

?>



</body>
</html>
