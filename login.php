<DOCTYPE! html>
<head>
<meta charset="UTF-8">
<title>Nav Details</title>
<style>
body {
    width: 800px;
    margin: 0 auto;
    padding: 0;
    font:12px/16px Verdana, sans-serif;
}
</style>
</head>
<body>

<?php
echo '<a href="main.php">Main</a>';
echo ' ';
session_start();
//temporary user login
//$_SESSION['user'] = "JoJo";
?>

<form action="login.php"  method="POST">
        <label for="user">Username:</label>
                <input type="text" name="user" id="user" required>
        <label for="pass">Password:</label>
                <input type="password" name="pass" id="pass" required>
                <input type="submit" value="Login">
</form>

<?php
//check if form has been filled
if(isset($_POST['pass'])){
if(isset($_POST['user'])){

$pass = $_POST['pass'];

//validate username
if(!preg_match('/^[\w_\-]+$/', $_POST['user']) ){
   echo '<label for="user">Provide Valid Username</label>';
   exit;
}

require 'database_connect.php';
$user = $_POST['user'];

//prepare statement to retrieve password(crypted)
$stmt = $mysqli->prepare("SELECT password FROM users WHERE user=?");

//bind to parameter
$stmt->bind_param('s', $user);
$stmt->execute();

//bind results (password hash)
$stmt->bind_result($pwd_hash);
$stmt->fetch();

echo $pwd_hash;
echo ' space ';
echo crypt($pass);

$stmt->close();

// Compare the submitted password to the actual password hash
if(crypt($pass, $pwd_hash)==$pwd_hash){
// Login succeeded!
$_SESSION['user'] = $_POST['user'];
/* Redirect to main page

     could replace 'main.php' in the header statement below with a variable
     stated at the top of each page with that page's filename, if we decide
     to save this whole section as a separate file to be included on other
     pages besides main.php (to provide a uniform navigation bar)
  */
header("Location: ".$page.".php");
exit;

}
else{
   echo 'Invalid Username or Password';
   exit;
}
}
}

?>

</body>
</html>